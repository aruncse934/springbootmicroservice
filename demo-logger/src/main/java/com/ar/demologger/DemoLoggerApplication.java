package com.ar.demologger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoLoggerApplication {
    private static final Logger logger = LoggerFactory.getLogger(DemoLoggerApplication.class);

    public static void main(String[] args) {
        logger.info( "This is a info messages!" );
        logger.warn("This is a warm messages");
        logger.error( "This is a Error Messages!" );
        SpringApplication.run( DemoLoggerApplication.class, args );


    }

}
