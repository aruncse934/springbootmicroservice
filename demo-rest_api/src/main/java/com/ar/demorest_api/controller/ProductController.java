package com.ar.demorest_api.controller;

import com.ar.demorest_api.model.Product;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ProductController {
    private static Map<String, Product> productRepo = new HashMap<>(  );
    static {
        Product computer = new Product();
        computer.setId("1");
        computer.setName( "Dell");
        productRepo.put( computer.getId(),computer );

        Product mobile = new Product();
        mobile.setId( "2" );
        mobile.setName( "OPPO" );
        productRepo.put( mobile.getId(),mobile );
    }
    @RequestMapping(value = "/products/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteProducts(@PathVariable("id") String id){
        productRepo.remove( id );
        return new ResponseEntity<>( "Product is deleted successfully", HttpStatus.OK );
    }
    @RequestMapping(value = "/products/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Product product){
        productRepo.remove(id);
        product.setId( id );
        productRepo.put( id,product );
        return new ResponseEntity<>( "Product is updated successfully",HttpStatus.OK );
    }
    @RequestMapping(value = "/products",method = RequestMethod.POST)
    public ResponseEntity<Object> addProduct(@RequestBody Product product){
        productRepo.put( product.getId(),product );
        return new ResponseEntity<>( "Product is created successfully",HttpStatus.CREATED );
    }
    @RequestMapping(value = "/products")
    public ResponseEntity<Object> getProduct(){
        return new ResponseEntity<>( productRepo.values(),HttpStatus.OK );
    }
}
/*
   @RequestMapping(value = "/products/{id}", method = RequestMethod.PUT)
   public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Product product) {


   }
}*/